<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    function render_page($content, $data = NULL){
    /*
     *
     * $data['contentnya'] variabel ini nantinya akan dikirim ke file views/template/index.php
     * */

        $data['contentnya'] = $this->load->view($content, $data, TRUE);

        $this->load->view('template/layout', $data);
    }

    function render_page_penilaian($content, $data = NULL){
        $data['contentnya'] = $this->load->view($content, $data, TRUE);
        $this->load->view('template/layout_penilaian', $data);
    }

}
