<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Laporan extends MY_Controller
{
    public function data_siswa(){
      $this->render_page('laporan/data_siswa');
    }

    public function get_siswa(){
      $kelas   = $this->input->post('konsentrasi');
      $this->db->join('tb_kelas', 'tb_kelas.id_kelas = tb_siswa.id_kelas');
      $get_siswa = $this->db->get_where('tb_siswa', array('tb_siswa.id_kelas' => $kelas))->result();

      echo json_encode(array('success' => true, 'data' => $get_siswa));
    }

    public function data_guru(){
      $this->render_page('laporan/data_guru');
    }

    public function get_guru(){
      $pangkat = $this->input->post('pangkat');
      $status  = $this->input->post('status');

      if($pangkat != 'semua'){
        $this->db->where('pangkat', $pangkat);
      }

      if($status != 'semua'){
        $this->db->where('status', $status);
      }

      $data = $this->db->get('tb_guru')->result();

      echo json_encode(array('success' => true, 'data' => $data));
    }

    public function data_penilaian(){
      $get  = $this->db->get('tb_periode')->result();

      $data['periode'] = $get;

      $data = array('data' => $data);
      $this->render_page('laporan/data_penilaian', $data);
    }

    public function lap_penilaian(){
      $periode = $this->input->post('periode');
      $get_period  = $this->db->get('tb_periode')->result();
      $get_hasil   = $this->db->query("SELECT * from tb_hasil_penilaian a JOIN tb_guru b ON a.nip = b.nip WHERE periode = '".$periode."' ")->result();

      $data['periode'] = $get_period;
      $data['hasil']   = $get_hasil;
      $data['post_periode'] = $periode;

      $data = array('data' => $data);
      $this->render_page('laporan/data_penilaian', $data);
    }

    public function cetak(){
      $periode = $this->uri->segment(3);
      $get_hasil   = $this->db->query("SELECT * from tb_hasil_penilaian a JOIN tb_guru b ON a.nip = b.nip WHERE periode = '".$periode."' ")->result();
      $thn_periode = $this->db->get_where("tb_periode", array('id_periode' => $periode))->row_array();
      $tahunnya    = $thn_periode['thn'];

      $data['hasil'] = $get_hasil;
      $data['tahun'] = $tahunnya;

      // $data = array('data' => $data);
      $this->render_page('laporan/pdf', $data);
    }
}
