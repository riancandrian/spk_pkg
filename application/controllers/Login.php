<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Login extends CI_Controller
{
  public function __construct(){
    parent::__construct();
      $this->load->model('m_login');

  }

    public function index()
    {
        $this->load->view('login/index');
    }

    function aksi_login(){
      $username = $this->input->post('username');
		  $password = $this->input->post('password');

      $param    = array('username' => $username, 'password' => $password);

      $cek = $this->m_login->cek_login($param)->num_rows();
		  $get = $this->m_login->cek_login($param)->row_array();

      if($cek > 0){
        $data_session = array(
  				'status' 	  => "login",
  				'user_id'   => $get['id_pengguna'],
          'username'  => $get['username'],
  				'role'      => $get['role']
				);

        $this->session->set_userdata($data_session);

        if($get['role'] == 'admin'){
          redirect('main');
        }else if($get['role'] == 'guru' || $get['role'] == 'siswa'){
          redirect('main/index_penilaian');
        }
      }else{
        $this->load->view('login/index', array('msg' => 'salah'));
      }
    }

    public function logout(){
      $this->session->sess_destroy();
      redirect('login');
  	}
}
