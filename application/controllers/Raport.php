<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Raport extends MY_Controller
{

  public function save_penilaian(){

    $get_periode = $this->db->get_where('tb_periode', array('status' => 'aktif' ))->row_array();
    $periode     = $get_periode['id_periode'];
    $idpengguna  = $this->session->userdata('user_id');
    $idguru      = $this->input->post('getGuru');

    $soal        = $this->input->post('soal');
    $insert      = "INSERT INTO tb_penilaian(`id_soal`, `id_periode`, `nilai`, `id_pengguna`, `nip`) VALUES ";
    $nomor       = 1;

    $soal        = $this->input->post('soal');

    foreach ($soal as $key => $value) {
      // code...
      $nilai = $this->input->post($nomor);
      $insert .= "('".$value."', '".$periode."', '".$nilai."', '".$idpengguna."', '".$idguru."'), ";
      $nomor++;
    }

    $query = substr($insert, 0, -2);

    //simpan
    $simpan = $this->db->query($query);

    if($simpan){
      echo json_encode(array('success' => true));
    }
  }

  public function rekap_penilaian(){
    $get_periode = $this->db->get_where('tb_periode', array('status' => 'aktif' ))->row_array();
    $periode     = $get_periode['id_periode'];

    # Untuk mempermudah dalam penyajian data, maka kita akan gunakan table temporary
    # tabel yang akan dibuat kita namakan "tb_temp"

    $this->db->query("DROP TABLE IF EXISTS t_temp");
    $this->db->query("
            CREATE TEMPORARY TABLE IF NOT EXISTS t_temp AS (
            SELECT nilai_siswa.nip
               , nilai_siswa.id_kriteria
               , IFNULL(nilai_siswa.nilai, 0) AS nilai_siswa
               , IFNULL(nilai_guru.nilai, 0) AS nilai_guru
            FROM
            (
            SELECT t_nilai.nip AS nip
                 , t_kriteria.id_kriteria AS id_kriteria
                 , sum(nilai) AS nilai
            FROM
              tb_penilaian t_nilai
            JOIN tb_soal_kompetensi t_soal
            ON t_soal.id_soal = t_nilai.id_soal
            JOIN tb_kompetensi t_kom
            ON t_kom.id_kompetensi = t_soal.id_kompetensi
            JOIN tb_kriteria t_kriteria
            ON t_kriteria.id_kriteria = t_kom.id_kriteria
            WHERE
              kategori = 'siswa'
              AND id_periode = '".$periode."'
            GROUP BY
              t_nilai.nip
            , t_kriteria.id_kriteria
            ) nilai_siswa
            LEFT JOIN (
            SELECT t_nilai.nip AS nip
               , t_kriteria.id_kriteria AS id_kriteria
               , sum(nilai) AS nilai
            FROM
            tb_penilaian t_nilai
            JOIN tb_soal_kompetensi t_soal
            ON t_soal.id_soal = t_nilai.id_soal
            JOIN tb_kompetensi t_kom
            ON t_kom.id_kompetensi = t_soal.id_kompetensi
            JOIN tb_kriteria t_kriteria
            ON t_kriteria.id_kriteria = t_kom.id_kriteria
            WHERE
            kategori = 'guru'
            AND id_periode = '".$periode."'
            GROUP BY
            t_nilai.nip
            , t_kriteria.id_kriteria
            ) nilai_guru
            ON nilai_siswa.nip = nilai_guru.nip AND nilai_siswa.id_kriteria = nilai_guru.id_kriteria
        )
    ");

    # buat tabel matrik
    $this->db->query("CREATE TEMPORARY TABLE IF NOT EXISTS t_matrik AS (SELECT nip
                    , max( CASE WHEN id_kriteria = 2 THEN (nilai_siswa + nilai_guru) END) pedagogik
                    , max( CASE WHEN id_kriteria = 3 THEN (nilai_siswa + nilai_guru) END) kepribadian
                    , max( CASE WHEN id_kriteria = 4 THEN (nilai_siswa + nilai_guru) END) sosial
                    , max( CASE WHEN id_kriteria = 5 THEN (nilai_siswa + nilai_guru) END) profesional
                    FROM t_temp
                    GROUP BY nip)");

    # ambil max dan min dari setiap kriteria
    $maxmin = $this->db->query("SELECT max(pedagogik) AS max_pedagostik
                         , min(pedagogik) AS min_pedagostik
                         , max(kepribadian) AS max_kepribadian
                         , min(kepribadian) AS min_kepribadian
                         , max(sosial) AS max_sosial
                         , min(sosial) AS min_sosial
                         , max(profesional) AS max_profesional
                         , min(profesional) AS min_profesional
                    FROM
                      t_matrik")->row_array();

    # ambil nilai bobot dari kriteria
    $bobot  = $this->db->query("SELECT * FROM tb_kriteria order by id_kriteria")->result();
    $n      = 1;
    $wrap   = array();

    foreach ($bobot as $value) {
      // code...
      $wrap[$value->kriteria] = $value->bobot;
      $n++;
    }

    $matrik = $this->db->query("SELECT * FROM t_matrik JOIN tb_guru ON tb_guru.nip = t_matrik.nip order by tb_guru.nip")->result();

    # bungkus data
    $data['matrik'] = $matrik;
    $data['maxmin'] = $maxmin;
    $data['bobot']  = $wrap;
    $data['periode']= $periode;

    $this->render_page('penilaian/index_rekap', $data);
  }

}
