<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Penilaian extends MY_Controller
{

  public function index_kriteria(){
    $this->render_page('penilaian/index_kriteria');
  }

  public function index_kompetensi(){
    $this->render_page('penilaian/index_kompetensi');
  }

  public function index_soal_kompetensi(){
    $this->render_page('penilaian/index_soal_kompetensi');
  }

  // CRITERIA
  function saveorup_kriteria(){
    $idk  = $this->input->post("idk");
    $data = array(
      'kriteria'  => $this->input->post('kriteria'),
      'bobot'     => $this->input->post('bobot'),
      'status'    => $this->input->post('status')
    );

    if($idk){
      $this->db->where('id_kriteria', $idk);
      $simpan = $this->db->update('tb_kriteria', $data);
    }else{
      $simpan = $this->db->insert('tb_kriteria', $data);
    }

    if($simpan){
      echo json_encode(array('success' => true));
    }

  }

  function getKriteria(){
    $idk = $this->input->post('idk');

    if($idk){
      $data = $this->db->get_where('tb_kriteria', array('id_kriteria' => $idk))->result();
    }else{
      $data = $this->db->get('tb_kriteria')->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusKriteria(){
    $where = array('id_kriteria' => $this->input->post('idk'));

    $this->db->delete('tb_kriteria', $where);

    echo json_encode(array('success' => true));
  }

  // KOMPETENSI
  function saveorup_kompetensi(){
    $idk  = $this->input->post("idk");
    $data = array(
      'id_kriteria'  => $this->input->post('kriteria'),
      'kompetensi'   => $this->input->post('kompetensi')
    );

    if($idk){
      $this->db->where('id_kompetensi', $idk);
      $simpan = $this->db->update('tb_kompetensi', $data);
    }else{
      $simpan = $this->db->insert('tb_kompetensi', $data);
    }

    if($simpan){
      echo json_encode(array('success' => true));
    }

  }

  function getKompetensi(){
    $idk = $this->input->post('idk');
    $this->db->join('tb_kriteria', "tb_kriteria.id_kriteria = tb_kompetensi.id_kriteria");

    if($idk){
      $data = $this->db->get_where('tb_kompetensi', array('id_kompetensi' => $idk))->result();
    }else{
      $data = $this->db->get('tb_kompetensi')->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusKompetensi(){
    $where = array('id_kompetensi' => $this->input->post('idk'));

    $this->db->delete('tb_kompetensi', $where);

    echo json_encode(array('success' => true));
  }

  // SOAL
  function saveorup_soal(){
    $ids  = $this->input->post("ids");
    $data = array(
      'id_kompetensi'  => $this->input->post('kompetensi'),
      'soal'   => $this->input->post('soal'),
      'kategori'   => $this->input->post('kategori')
    );

    if($ids){
      $this->db->where('id_soal', $ids);
      $simpan = $this->db->update('tb_soal_kompetensi', $data);
    }else{
      $simpan = $this->db->insert('tb_soal_kompetensi', $data);
    }

    if($simpan){
      echo json_encode(array('success' => true));
    }

  }

  function getSoal(){
    $ids = $this->input->post('ids');
    $this->db->join('tb_kompetensi', "tb_soal_kompetensi.id_kompetensi = tb_kompetensi.id_kompetensi");

    if($ids){
      $data = $this->db->get_where('tb_soal_kompetensi', array('id_soal' => $ids))->result();
    }else{
      $data = $this->db->get('tb_soal_kompetensi')->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusSoal(){
    $where = array('id_soal' => $this->input->post('ids'));

    $this->db->delete('tb_soal_kompetensi', $where);

    echo json_encode(array('success' => true));
  }

  public function getsoal_fortest(){
    $role = $this->session->userdata('role');
    $this->db->where('kategori', $role);
    $data = $this->db->get('tb_soal_kompetensi')->result();

    echo json_encode(array('success' => true, 'data' => $data));
  }

}
