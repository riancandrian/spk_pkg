<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Setting extends MY_Controller
{

  public function index(){
      $this->render_page('setting/index_sekolah');
  }

  public function index_periode(){
    $this->render_page('setting/index_periode');
  }

  public function saveorup_company(){

    $data = array('nama' => $this->input->post('nama')
                , 'telp' => $this->input->post('telp')
                , 'alamat' => $this->input->post('alamat')
              );

    $update = $this->db->update('tb_company', $data);
    if($update){
      $msg  = "Data berhasil di update";
      $data = array('success' => true, 'msg' => $msg);
      $this->render_page('setting/index_sekolah', $data);
    }
  }

  public function saveorup_periode(){
    $id_periode = $this->input->post('id_periode');
    $data = array(
      'thn'   => $this->input->post('thn'),
      'start' => $this->input->post('start'),
      'end'   => $this->input->post('end'),
      'status' => $this->input->post('status')
    );

    if($this->input->post('status') == 'aktif'){
      $cek = $this->db->get_where('tb_periode', array('status' => 'aktif'))->num_rows();

      if($cek > 0){
        echo json_encode(array('success' => false));
      }else{
        if($id_periode){
          $this->db->where('id_periode', $id_periode);
          $simpan = $this->db->update('tb_periode', $data);
        }else{
          $simpan = $this->db->insert('tb_periode', $data);
        }

        if($simpan){
          echo json_encode(array('success' => true));
        }
      }
    }else{
      if($id_periode){
        $this->db->where('id_periode', $id_periode);
        $simpan = $this->db->update('tb_periode', $data);
      }else{
        $simpan = $this->db->insert('tb_periode', $data);
      }

      if($simpan){
        echo json_encode(array('success' => true));
      }
    }  
  }

  public function getPeriode(){
    $id_periode = $this->input->post('id_periode');

    if($id_periode){
      $this->db->where('id_periode', $id_periode);
      $data = $this->db->get("tb_periode")->result();
    }else{
      $data = $this->db->get("tb_periode")->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusPeriode(){
    $where = array('id_periode' => $this->input->post('id_periode'));

    $this->db->delete('tb_periode', $where);

    echo json_encode(array('success' => true));
  }
}
