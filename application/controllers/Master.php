<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Master extends MY_Controller
{

  public function index_kelas(){
      $this->render_page('master/index_kelas');
  }

  public function index_siswa(){
      $this->render_page('master/index_siswa');
  }

  public function index_guru(){
      $this->render_page('master/index_guru');
  }

  public function index_pengguna(){
      $this->render_page('master/index_pengguna');
  }

  // FUNGSI KELAS
  public function saveorup_kelas(){
    $id_kelas = $this->input->post('id_kelas');
    $data = array('tingkat' => $this->input->post('tingkat'), 'konsentrasi' => $this->input->post('konsentrasi') );

    if($id_kelas){
      $this->db->where('id_kelas', $id_kelas);
      $simpan = $this->db->update('tb_kelas', $data);
    }else{
      $simpan = $this->db->insert('tb_kelas', $data);
    }

    if($simpan){
      echo json_encode(array('success' => true));
    }
  }

  public function getKelas(){
    $id_kelas = $this->input->post('id_kelas');
    $tingkat  = $this->input->post('tingkat');

    $this->db->order_by('konsentrasi');
    if($id_kelas){
      $this->db->where('id_kelas', $id_kelas);
      $data = $this->db->get("tb_kelas")->result();

    }else if($tingkat){
      $this->db->where('tingkat', $tingkat);
      $data = $this->db->get("tb_kelas")->result();

    }else{
      $data = $this->db->get("tb_kelas")->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusKelas(){
    $where = array('id_kelas' => $this->input->post('id_kelas'));

    $this->db->delete('tb_kelas', $where);

    echo json_encode(array('success' => true));
  }

  // FUNGSI SISWA
  public function getSiswa(){
    $data = $this->db->query("SELECT * from tb_siswa
            JOIN tb_kelas ON tb_kelas.id_kelas = tb_siswa.id_kelas")->result();
    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function saveorup_siswa(){
    $nis  = $this->input->post('nis');

    $cNis = $this->db->get_where('tb_siswa', array('nis' => $nis))->num_rows();

    if($cNis > 0){
      $data = array('nama' => $this->input->post('nama'), 'id_kelas' => $this->input->post('konsentrasi'));

      $this->db->where('nis', $nis);
      $simpan = $this->db->update('tb_siswa', $data);

    }else{
      $data = array('nama' => $this->input->post('nama')
                  , 'nis'  => $this->input->post('nis')
                  , 'id_kelas' => $this->input->post('konsentrasi')
                );

      $simpan = $this->db->insert('tb_siswa', $data);
    }

    if($simpan){
      echo json_encode(array('success' => true));
    }
  }

  public function hapusSiswa(){
    $where = array('nis' => $this->input->post('nis'));

    $this->db->delete('tb_siswa', $where);

    echo json_encode(array('success' => true));
  }

  // FUNGSI Guru
  public function saveorup_guru(){
    $nip  = $this->input->post('nip');

    $cNip = $this->db->get_where('tb_guru', array('nip' => $nip))->num_rows();

    if($cNip > 0){

      $data = array('nama_guru' => $this->input->post('nama_guru')
                  , 'status' => $this->input->post('status')
                  , 'pangkat' => $this->input->post('pangkat')
                  , 'tgl_mulai_kerja' => $this->input->post('tgl_mulai_kerja')
                );

      $this->db->where('nip', $nip);
      $simpan = $this->db->update('tb_guru', $data);

    }else{
      $data = array('nama_guru' => $this->input->post('nama_guru')
                  , 'nip'  => $this->input->post('nip')
                  , 'status' => $this->input->post('status')
                  , 'pangkat' => $this->input->post('pangkat')
                  , 'tgl_mulai_kerja' => $this->input->post('tgl_mulai_kerja')
                );

      $simpan = $this->db->insert('tb_guru', $data);
    }

    if($simpan){
      echo json_encode(array('success' => true));
    }
  }

  public function getGuru(){
    $nip  = $this->input->post('nip');

    if($nip){
      $this->db->where('nip', $nip);
      $data = $this->db->get("tb_guru")->result();
    }else{
      $data = $this->db->get("tb_guru")->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusGuru()
  {
    $where = array('nip' => $this->input->post('nip'));

    $this->db->delete('tb_guru', $where);

    echo json_encode(array('success' => true));
  }

  // FUNGSI Pengguna
  public function saveorup_pengguna(){
    $idp  = $this->input->post('id_pengguna');

    $cIdp = $this->db->get_where('tb_pengguna', array('id_pengguna' => $idp))->num_rows();

    if($cIdp > 0){
      $username   = $this->input->post('username');
      $role       = $this->input->post('role');
      $status     = $this->input->post('status');
      $nama_lengkap = $this->input->post('nama_lengkap');
      $pass       = $this->input->post('password');

      $query      = "UPDATE tb_pengguna SET username = '".$username."', role = '".$role."', status = '".$status."', nama_lengkap = '".$nama_lengkap."' ";

      if($pass){
        $query    .= ", password = sha('".$pass."') ";
      }

      $query      .= " WHERE id_pengguna = '".$idp."' ";
      $simpan     = $this->db->query($query);

    }else{
      $username = $this->input->post('username');
      $role     = $this->input->post('role');
      $status   = $this->input->post('status');
      $nama_lengkap = $this->input->post('nama_lengkap');
      $pass     = $this->input->post('password');

      $simpan   = $this->db->query("INSERT INTO tb_pengguna VALUES('', '".$username."', sha('".$pass."'), '".$nama_lengkap."', '".$role."', '".$status."')"
      );

    }


    if($simpan){
      echo json_encode(array('success' => true));
    }
  }

  public function getNamaPengguna(){
    $username = $this->input->post('username');
    $role     = $this->input->post('role');

    if($role == 'siswa'){
      $get  = $this->db->get_where('tb_siswa', array('nis' => $username))->row_array();
      $user = $get['nama'];
    }else if($role == 'guru'){
      $get  = $this->db->get_where('tb_guru', array('nip' => $username))->row_array();
      $user = $get['nama_guru'];
    }else{
      $user = "Silahkan masukan nama";
    }

    echo json_encode(array('success' => true, 'nama' => $user));

  }

  public function getPengguna(){
    $idp  = $this->input->post('idp');

    if($idp){
      $this->db->where('id_pengguna', $idp);
      $data = $this->db->get("tb_pengguna")->result();
    }else{
      $data = $this->db->get("tb_pengguna")->result();
    }

    echo json_encode(array('success' => true, 'data' => $data));
  }

  public function hapusPengguna(){
    $where = array('id_pengguna' => $this->input->post('idp'));

    $this->db->delete('tb_pengguna', $where);

    echo json_encode(array('success' => true));
  }

}
