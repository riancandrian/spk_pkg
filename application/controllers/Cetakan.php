<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cetakan extends CI_Controller {

  public function __construct(){
    parent::__construct();
      if($this->session->userdata('status') != "login"){
        redirect('login');
      }
  }

  public function index()
  {
    $periode = $this->uri->segment(3);
    $get_hasil   = $this->db->query("SELECT * from tb_hasil_penilaian a JOIN tb_guru b ON a.nip = b.nip WHERE periode = '".$periode."' ")->result();
    $thn_periode = $this->db->get_where("tb_periode", array('id_periode' => $periode))->row_array();
    $tahunnya    = $thn_periode['thn'];

    $data['hasil'] = $get_hasil;
    $data['tahun'] = $tahunnya;

    // $data = array('data' => $data);
    $this->load->view('laporan/pdf_penilaian', $data);
  }
}
