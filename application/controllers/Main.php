<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Main extends MY_Controller
{
  public function __construct(){
    parent::__construct();
      // $this->load->model('m_data');
      if($this->session->userdata('status') != "login"){
        redirect('login');
      }
  }

  public function index(){
    $getperiode       = $this->db->query("SELECT * from tb_periode WHERE status = 'aktif'")->row_array();
    $id_periode       = $getperiode['id_periode'];
    $data['pengguna'] = $this->db->query("SELECT * from tb_pengguna WHERE status = 'active' ")->num_rows();
    $data['siswa']    = $this->db->query("SELECT * from tb_siswa")->num_rows();
    $data['guru']     = $this->db->query("SELECT * from tb_guru WHERE status = 'active'")->num_rows();
    $data['guru_chart'] = $this->db->query("SELECT * from tb_guru WHERE status = 'active'")->result();
    $data['periode']  = $getperiode;
    $data['penilaian']= $this->db->query("SELECT skor from tb_hasil_penilaian where periode = '".$id_periode."' ")->result();
    $this->render_page('dashboard', $data);
  }

  public function index_penilaian(){
    $this->render_page_penilaian('penilaian/input');
  }

}
