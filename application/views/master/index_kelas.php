<div class="content-wrapper">
  <section class="content-header">
    <h1>Master Kelas</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-database"></i> Master</a></li>
      <li class="active">Kelas</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i> Tambah</button>
        </div>
      </div>

      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-hover" id="tb_kelas">
            <thead>
              <th width="10%">No.</th>
              <th width="20%">Tingkat</th>
              <th width="50%">Konsentrasi</th>
              <th width="20%"></th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" id="f_kelas">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Kelas</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="col-md-3 control-label">Tingkat</label>
            <div class="col-md-8">
              <input type="hidden" name="id_kelas" value="">
              <select class="form-control" name="tingkat" required>
                <option value="">Pilih Tingkat..</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Konsentrasi</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="konsentrasi" required>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    loadData();

    $('form#f_kelas').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'master/saveorup_kelas',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);

            if(jsonData.success){
                Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
                loadData();
                $('#modal-default').modal('hide');
                $('form#f_kelas').trigger('reset');
            }else{
                alert("Data Gagal disimpan");
                $('form#f_kelas').triger('reset');
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });
  });

  function loadData(){
    $.ajax({
      url : url + 'master/getKelas',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        $('#tb_kelas tbody').empty();
        var nomor = 1;

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){

            btn  = "<button type='button' class='btn btn-xs btn-warning' onclick='getForEdit("+val.id_kelas+")'><i class='fa fa-edit'></i> Edit</button>";
            btn2 = "<button type='button' class='btn btn-xs btn-danger' onclick='hapus("+val.id_kelas+")'><i class='fa fa-trash'></i> Hapus</button>";

            var tr = "<tr>\
                        <td>"+(nomor++)+"</td>\
                        <td>"+val.tingkat+"</td>\
                        <td>"+val.konsentrasi+"</td>\
                        <td>"+btn+' '+btn2+"</td>\
                      </tr>";

            $('#tb_kelas tbody').append(tr);

          });
        }
      }
    });
  }

  function hapus(id_kelas){
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Anda tidak bisa membatalkan aksi ini!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus saja!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url : url + 'master/hapusKelas',
          data: {id_kelas: id_kelas},
          type: 'POST',
          success: function(result){
            var jsonData = JSON.parse(result);

            if(jsonData.success){
              Swal.fire(
                'Deleted!',
                'Record telah terhapus.',
                'success'
              )

              loadData();
            }
          }
        });
      }
    })
  }

  function getForEdit(id_kelas){
     $.ajax({
       url : url + 'master/getKelas',
       data: {id_kelas: id_kelas},
       type: 'POST',
       success: function(result){
         var jsonData = JSON.parse(result);

         if(jsonData.success){
           $.each(jsonData.data, function(key, val){
             $('input[name="id_kelas"]').val(val.id_kelas);
             $('select[name="tingkat"]').val(val.tingkat);
             $('input[name="konsentrasi"]').val(val.konsentrasi);
           });
         }

         $('#modal-default').modal('show');
       }
     });
  }
</script>
