<div class="content-wrapper">
  <section class="content-header">
    <h1>Master Guru</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-database"></i> Master</a></li>
      <li class="active">Guru</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i> Tambah</button>
        </div>
      </div>

      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-hover" id="tb_guru">
            <thead>
              <th width="5%">No.</th>
              <th width="15%">NIP</th>
              <th width="20%">Nama Guru</th>
              <th width="15%">Pangkat</th>
              <th width="15%">Tgl. Kerja</th>
              <th width="10%">Status</th>
              <th width="20%"></th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>


<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" method="post" id="f_guru">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Guru</h4>
        </div>
        <div class="modal-body">

          <div class="form-group">
            <label class="col-md-3 control-label">NIP</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="nip" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Nama Guru</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="nama_guru" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Pangkat</label>
            <div class="col-md-8">
              <select class="form-control" name="pangkat">
                <option value="">Pilih Tingkat..</option>
                <option value="tetap">Guru Tetap</option>
                <option value="honorer">Guru Honorer</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Tgl. Mulai Kerja</label>
            <div class="col-md-8">
              <input type="date" class="form-control" name="tgl_mulai_kerja" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-8">
              <select class="form-control" name="status">
                <option value="">Pilih Status..</option>
                <option value="active">Active</option>
                <option value="nonactive">Non Active</option>
              </select>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>


<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    loadData();

    $('form#f_guru').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'master/saveorup_guru',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);

            if(jsonData.success){
                Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
                loadData();
                $('form#f_guru').trigger('reset');
                $('#modal-default').modal('hide');
            }else{
                alert("Data Gagal disimpan");
                $('form#f_guru').triger('reset');
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });
  });

  function loadData(){
    $.ajax({
      url : url + 'master/getGuru',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        $('#tb_guru tbody').empty();
        var pangkat = '';
        var label = '';
        var nomor = 1;

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){

            btn  = "<button type='button' class='btn btn-xs btn-warning' onclick=\"getForEdit('"+val.nip+"')\"><i class='fa fa-edit'></i> Edit</button>";
            btn2 = "<button type='button' class='btn btn-xs btn-danger' onclick=\"hapus('"+val.nip+"')\"><i class='fa fa-trash'></i> Hapus</button>";

            if(val.pangkat == 'tetap'){
              pangkat = 'Guru Tetap';
            }else{
              pangkat = 'Guru Honorer';
            }

            if(val.status == 'active'){
              label = '<small class="label bg-green">Aktif</small>';
            }else{
              label = '<small class="label bg-red">Non Aktif</small>';
            }

            var tr = "<tr>\
                        <td>"+(nomor++)+"</td>\
                        <td>"+val.nip+"</td>\
                        <td>"+val.nama_guru+"</td>\
                        <td>"+pangkat+"</td>\
                        <td>"+val.tgl_mulai_kerja+"</td>\
                        <td>"+label+"</td>\
                        <td>"+btn+' '+btn2+"</td>\
                      </tr>";

            $('#tb_guru tbody').append(tr);

          });
        }
      }
    });
  }

  function getForEdit(nip){
     $.ajax({
       url : url + 'master/getGuru',
       data: {nip: nip},
       type: 'POST',
       success: function(result){
         var jsonData = JSON.parse(result);

         if(jsonData.success){
           $.each(jsonData.data, function(key, val){
             $('input[name="nip"]').val(val.nip);
             $('input[name="nama_guru"]').val(val.nama_guru);
             $('input[name="tgl_mulai_kerja"]').val(val.tgl_mulai_kerja);
             $('select[name="pangkat"]').val(val.pangkat);
             $('select[name="status"]').val(val.status);

           });
         }

         $('#modal-default').modal('show');
       }
     });
  }

  function hapus(nip){
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Anda tidak bisa membatalkan aksi ini!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus saja!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url : url + 'master/hapusGuru',
          data: {nip: nip},
          type: 'POST',
          success: function(result){
            var jsonData = JSON.parse(result);

            if(jsonData.success){
              Swal.fire(
                'Deleted!',
                'Record telah terhapus.',
                'success'
              )

              loadData();
            }
          }
        });
      }
    })
  }
</script>
