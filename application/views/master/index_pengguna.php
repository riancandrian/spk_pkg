<div class="content-wrapper">
  <section class="content-header">
    <h1>Master Pengguna</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-database"></i> Master</a></li>
      <li class="active">Pengguna</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i> Tambah</button>
        </div>
      </div>

      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-hover" id="tb_pengguna">
            <thead>
              <th width="5%">No.</th>
              <th width="20%">Username</th>
              <th width="30%">Nama</th>
              <th width="15%">Level</th>
              <th width="10%">Status</th>
              <th width="20%"></th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" method="post" id="f_pengguna">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Pengguna</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="col-md-3 control-label">Level</label>
            <div class="col-md-8">
              <select class="form-control" name="role">
                <option value="">Pilih level..</option>
                <option value="guru">Guru</option>
                <option value="siswa">Siswa</option>
                <option value="kepsek">Kepala Sekolah</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Username</label>
            <div class="col-md-8">
              <input type="hidden" name="id_pengguna" value="">
              <input type="text" class="form-control" name="username" placeholder="NIP / NIS">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Nama Lengkap</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="nama_lengkap" readonly>
            </div>
          </div>

          <div class="form-group" id="div_pass">
            <label class="col-md-3 control-label">Password</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="password" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-8">
              <select class="form-control" name="status">
                <option value="">Pilih Status..</option>
                <option value="active">Aktif</option>
                <option value="nonactive">Non Aktif</option>
              </select>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  loadData();

  $('input[name="username"]').change(function(){
    var username = $(this).val();
    var role     = $('select[name="role"]').val();

    $.ajax({
      url : url + 'master/getNamaPengguna',
      type: 'POST',
      data: {username: username, role: role},
      success: function(result){
        var jsonData = JSON.parse(result);

        if(jsonData.success){
          $('input[name="nama_lengkap"]').val(jsonData.nama);
        }
      }
    });
  });

  $('form#f_pengguna').submit(function(e){
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
      url: url + 'master/saveorup_pengguna',
      type: 'POST',
      data: formData,
      success: function (data) {
          var jsonData = JSON.parse(data);

          if(jsonData.success){
              Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
              loadData();
              $('#modal-default').modal('hide');
              $('form#f_pengguna').trigger('reset');
          }else{
              alert("Data Gagal disimpan");
              $('form#f_pengguna').trigger('reset');
          }
      },
      cache: false,
      contentType: false,
      processData: false
    });
  });
});

function loadData(){
  $.ajax({
    url : url + 'master/getPengguna',
    type: 'POST',
    success: function(result){
      var jsonData = JSON.parse(result);
      var nomor = 1;
      $('#tb_pengguna tbody').empty();

      if(jsonData.success){
        $.each(jsonData.data, function(key, val){
          btn  = "<button type='button' class='btn btn-xs btn-warning' onclick='getForEdit("+val.id_pengguna+")'><i class='fa fa-edit'></i> Edit</button>";
          btn2 = "<button type='button' class='btn btn-xs btn-danger' onclick='hapus("+val.id_pengguna+")'><i class='fa fa-trash'></i> Hapus</button>";

          if(val.status == 'active'){
            label = '<small class="label bg-green">Aktif</small>';
          }else{
            label = '<small class="label bg-red">Non Aktif</small>';
          }

          var tr = "<tr>\
                      <td>"+(nomor++)+"</td>\
                      <td>"+val.username+"</td>\
                      <td>"+val.nama_lengkap+"</td>\
                      <td>"+val.role+"</td>\
                      <td>"+label+"</td>\
                      <td>"+btn+' '+btn2+"</td>\
                    </tr>";

          $('#tb_pengguna tbody').append(tr);

        });
      }
    }
  });

}

function hapus(idp){
  Swal.fire({
    title: 'Apakah Anda yakin?',
    text: "Anda tidak bisa membatalkan aksi ini!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Ya, hapus saja!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url : url + 'master/hapusPengguna',
        data: {idp: idp},
        type: 'POST',
        success: function(result){
          var jsonData = JSON.parse(result);

          if(jsonData.success){
            Swal.fire(
              'Deleted!',
              'Record telah terhapus.',
              'success'
            )

            loadData();
          }
        }
      });
    }
  })
}

function getForEdit(idp){
   $.ajax({
     url : url + 'master/getPengguna',
     data: {idp: idp},
     type: 'POST',
     success: function(result){
       var jsonData = JSON.parse(result);

       if(jsonData.success){
         $.each(jsonData.data, function(key, val){
           $('select[name="role"]').val(val.role);
           $('input[name="username"]').val(val.username);
           $('input[name="nama_lengkap"]').val(val.nama_lengkap);
           $('input[name="password"]').attr('placeholder', "Kosongkan jika tidak ingin dirubah");
           $('select[name="status"]').val(val.status);
           $('input[name="id_pengguna"]').val(val.id_pengguna);
         });
       }

       $('#modal-default').modal('show');
     }
   });
}
</script>
