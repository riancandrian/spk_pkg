<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SPK PKG</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" type="image/png" href="<?=base_url('assets/login/'); ?>images/icons/favicon_.ico"/>
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/select2/dist/css/select2.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?=base_url(); ?>assets/plugins/iCheck/all.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <input type="hidden" value="<?=base_url(); ?>" id="base_url">
  <script type="text/javascript">
    var url = '<?php echo base_url(); ?>';
  </script>
</head>
<body class="hold-transition skin-red sidebar-mini">

<div class="wrapper">
  <header class="main-header">
    <a href="<?=base_url('main')?>" class="logo">
      <span class="logo-mini"><b>SPK</b></span>
      <span class="logo-lg"><b>SPK</b> PKG</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="<?=base_url();?>login/logout"><i class="fa fa-sign-out"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <?php
            echo '<img src="'.base_url().'assets/login/images/boy.png" class="img-circle" alt="User Image">';
          ?>

        </div>
        <div class="pull-left info">
          <p><?php echo mb_strtoupper($_SESSION['username']);?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <form class="sidebar-form" action="#">

      </form>

      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU UTAMA</li>

        <li><a href="<?=base_url();?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('setting/');?>"><i class="fa fa-minus active"></i> Sekolah</a></li>
            <li><a href="<?=base_url('setting/index_periode');?>"><i class="fa fa-minus"></i> Periode</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('master/index_siswa');?>"><i class="fa fa-minus"></i> Siswa</a></li>
            <li><a href="<?=base_url('master/index_guru');?>"><i class="fa fa-minus"></i> Guru</a></li>
            <li><a href="<?=base_url('master/index_pengguna');?>"><i class="fa fa-minus"></i> Pengguna</a></li>
            <li><a href="<?=base_url('master/index_kelas');?>"><i class="fa fa-minus"></i> <span>Kelas</span></a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list"></i> <span>Parameter Penilaian</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('penilaian/index_kriteria');?>"><i class="fa fa-minus"></i> Kriteria</a></li>
            <li><a href="<?=base_url('penilaian/index_kompetensi');?>"><i class="fa fa-minus"></i> Kompetensi</a></li>
            <li><a href="<?=base_url('penilaian/index_soal_kompetensi');?>"><i class="fa fa-minus"></i> Soal Kompetensi</a></li>
          </ul>
        </li>

        <li><a href="<?=base_url('raport/rekap_penilaian');?>"><i class="fa fa-check-square-o"></i> <span>Rekap Penilaian</span></a></li>
        <!-- <li><a href="<?=base_url('main/index_penilaian');?>"><i class="fa fa-check-square-o"></i> <span>Input Penilaian</span></a></li> -->

        <li class="treeview">
          <a href="#">
            <i class="fa fa-book"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url('laporan/data_siswa');?>"><i class="fa fa-minus"></i> Data Siswa</a></li>
            <li><a href="<?=base_url('laporan/data_guru');?>"><i class="fa fa-minus"></i> Data Guru</a></li>
            <li><a href="<?=base_url('laporan/data_penilaian');?>"><i class="fa fa-minus"></i> Hasil Penilaian</a></li>
          </ul>
        </li>

      </ul>
    </section>
  </aside>

  <!-- =============================================== -->
  <?=$contentnya; ?>

  <footer class="main-footer">
    <?php

      $get = $this->db->get('tb_company')->row_array();


    ?>
    <div class="pull-right hidden-xs">
      <b></b> <?=$get['alamat']?>
    </div>
    <strong>Copyright &copy; 2019 <a href="#"><?=$get['nama']?></a>.</strong>
  </footer>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/demo.js"></script>
<script src="<?=base_url(); ?>assets/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?=base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script>

  $(document).ready(function () {
    var url = $('#base_url').val();

    $('.sidebar-menu').tree();

    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })

  });
</script>
</body>
</html>
