

<div class="content-wrapper">
  <section class="content-header">
    <h1>Setting Periode</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
      <li class="active">Periode</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
      </div>

      <div class="box-body">
        <form class="form-horizontal" method="post" id="f_periode">
          <div class="form-group">
            <label class="col-md-3 control-label">Tahun Periode</label>
            <div class="col-md-6">
              <input type="hidden" name="id_periode" value="">
              <input type="text" class="form-control" name="thn" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Tgl Awal Berlaku</label>
            <div class="col-md-6">
              <input type="date" class="form-control" name="start" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Tgl Akhir Berlaku</label>
            <div class="col-md-6">
              <input type="date" class="form-control" name="end" value="">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-3">
              <input type="radio" class="minimal-red" name="status" value="aktif"> Aktif
            </div>

            <div class="col-md-3">
              <input type="radio" class="minimal-red" name="status" value="nonaktif"> Non Aktif
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3"></label>
            <div class="col-md-6">
              <button type="submit" class="btn bg-danger"><i class="fa fa-save"></i> Simpan</button>
            </div>
          </div>
        </form>

        <br><br>

        <div class="table-responsive">
          <table class="table table-hover" id="tb_periode">
            <thead>
              <th width="10%">No.</th>
              <th width="20%">Tahun</th>
              <th width="20%">Tgl. Awal</th>
              <th width="20%">Tgl. Akhir</th>
              <th width="10%">Status</th>
              <th width="20%"></th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    loadData();

    $('form#f_periode').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'setting/saveorup_periode',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);

            if(jsonData.success){
                Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
                loadData();
            }else{
                Swal.fire("Maaf !", "Periode Aktif hanya boleh 1", "alert");
                // $('form#f_periode').triger('reset');
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });

  });

  function loadData(){
    $.ajax({
      url : url + 'setting/getPeriode',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);
        var nomor = 1;
        var label = '';
        var btn   = '';
        var btn2  = '';

        $('#tb_periode tbody').empty();

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){

            if(val.status == 'aktif'){
              label = '<small class="label bg-green">Aktif</small>';
            }else{
              label = '<small class="label bg-red">Non Aktif</small>';
            }

            btn  = "<button type='button' class='btn btn-xs btn-warning' onclick='getForEdit("+val.id_periode+")'><i class='fa fa-edit'></i> Edit</button>";
            btn2 = "<button type='button' class='btn btn-xs btn-danger' onclick='hapus("+val.id_periode+")'><i class='fa fa-trash'></i> Hapus</button>";

            var tr = "<tr>\
                        <td>"+(nomor++)+"</td>\
                        <td>"+val.thn+"</td>\
                        <td>"+val.start+"</td>\
                        <td>"+val.end+"</td>\
                        <td>"+label+"</td>\
                        <td>"+btn+' '+btn2+"</td>\
                      </tr>";

            $('#tb_periode tbody').append(tr);

          });
        }
      }
    });
  }

  function hapus(id_periode){
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Anda tidak bisa membatalkan aksi ini!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus saja!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url : url + 'setting/hapusPeriode',
          data: {id_periode: id_periode},
          type: 'POST',
          success: function(result){
            var jsonData = JSON.parse(result);

            if(jsonData.success){
              Swal.fire(
                'Deleted!',
                'Record telah terhapus.',
                'success'
              )

              loadData();
            }
          }
        });
      }
    })
  }

  function getForEdit(id_periode){
     $.ajax({
       url : url + 'setting/getPeriode',
       data: {id_periode: id_periode},
       type: 'POST',
       success: function(result){
         var jsonData = JSON.parse(result);

         if(jsonData.success){
           $.each(jsonData.data, function(key, val){
             $('input[name="id_periode"]').val(val.id_periode);
             $('input[name="thn"]').val(val.thn);
             $('input[name="start"]').val(val.start);
             $('input[name="end"]').val(val.end);
             // $('input[name="status"]').val(val.status);

             if ( val.status === "aktif" ){
                 $(':radio[name=status][value=aktif]').iCheck('check');
             } else {
                 $(':radio[name=status][value=nonaktif]').iCheck('check');
             }
           });
         }
       }
     });
  }

</script>
