<div class="content-wrapper">
  <section class="content-header">
    <h1>Setting Sekolah</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-cogs"></i> Setting</a></li>
      <li class="active">Sekolah</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">

      </div>

      <?php

        $get = $this->db->get('tb_company')->row_array();

      ?>

      <div class="box-body">
        <?php

          if(isset($msg)){
            echo '
              <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check-square-o"></i> Info</h4>
                '.$msg.' !
              </div>
            ';
          }

        ?>
        <form class="form-horizontal" action="<?=base_url('setting/saveorup_company')?>" method="post">
          <div class="form-group">
            <label class="col-md-3 control-label">Nama Sekolah</label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="nama" value="<?=$get['nama']?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">No. Telp</label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="telp" value="<?=$get['telp']?>">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Alamat</label>
            <div class="col-md-6">
              <textarea name="alamat" class="form-control" rows="4" cols="80"><?=$get['alamat']?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3"></label>
            <div class="col-md-6">
              <button type="submit" class="btn bg-danger"><i class="fa fa-save"></i> Simpan</button>
            </div>
          </div>
        </form>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
