<div class="content-wrapper">
  <section class="content-header">
    <h1>Laporan Data Guru</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-book"></i> Laporan</a></li>
      <li class="active">Data Guru</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <!-- <button type="button" class="btn bg-danger btn-sm btn-flat" ><i class="fa fa-print"></i> Cetak</button> -->
        </div>
      </div>

      <div class="box-body">
        <form class="form-horizontal" id="f_guru" method="post">
          <div class="form-group">
            <label class="col-md-2 control-label">Pangkat</label>
            <div class="col-md-4">
              <select class="form-control" name="pangkat">
                <option value="semua">Semua</option>
                <option value="tetap">Tetap</option>
                <option value="honorer">Honorer</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 control-label">Status</label>
            <div class="col-md-4">
              <select class="form-control" name="status">
                <option value="semua">Semua</option>
                <option value="active">Aktif</option>
                <option value="nonactive">Non Aktif</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-8">
              <button type="submit" name="submit" class="btn btn-danger"><i class="fa fa-search"></i> Tampilkan</button>
            </div>
          </div>
        </form>
        <br>
        <div class="table-responsive">
          <table class="table table-hover" id="tb_guru">
            <thead>
              <th width="5%">No.</th>
              <th width="15%">NIP</th>
              <th width="20%">Nama Guru</th>
              <th width="15%">Pangkat</th>
              <th width="15%">Tgl. Kerja</th>
              <th width="10%">Status</th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('form#f_guru').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'laporan/get_guru',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);
            var nomor = 1;
            $('#tb_guru tbody').empty();

            if(jsonData.success){
              $.each(jsonData.data, function(key, val){

                var tr = "<tr>\
                            <td>"+(nomor++)+"</td>\
                            <td>"+val.nip+"</td>\
                            <td>"+val.nama_guru+"</td>\
                            <td>"+val.pangkat+"</td>\
                            <td>"+val.tgl_mulai_kerja+"</td>\
                            <td>"+val.status+"</td>\
                          </tr>";

                $('#tb_guru tbody').append(tr);

              });
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });
  });
</script>
