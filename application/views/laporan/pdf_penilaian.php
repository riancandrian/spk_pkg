<?php
    $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
    $pdf->SetTitle('Hasil Penilaian');
    $pdf->SetTopMargin(20);
    $pdf->setFooterMargin(20);
    $pdf->SetAutoPageBreak(true);
    $pdf->SetAuthor('Author');
    $pdf->SetDisplayMode('real', 'default');
    $pdf->AddPage();

    $i=0;
    $html='<h3 align="center">Daftar Hasil Penilaian Kinerja Guru</h3>
            <h3 align="center">Periode '.$tahun.'</h3>
            <table cellspacing="1" bgcolor="#666666" cellpadding="2">
              <tr bgcolor="#ffffff">
                <th width="5%" rowspan="2">No.</th>
                <th width="10%" rowspan="2">NIP</th>
                <th width="20%" rowspan="2">Nama Guru</th>
                <th width="10%" rowspan="2">Tgl. Kerja</th>
                <th width="10%" rowspan="2">Pangkat</th>
                <th width="45%" colspan="5">Score</th>
              </tr>
              <tr bgcolor="#ffffff">
                <th>Pedagogik</th>
                <th>Kepribadian</th>
                <th>Sosial</th>
                <th>Profesional</th>
                <th>Total</th>
              </tr>';
      foreach ($hasil as $row)
      {
          $i++;
          $html.='<tr bgcolor="#ffffff">
                    <td align="center">'.$i.'</td>
                    <td>'.$row->nip.'</td>
                    <td>'.$row->nama_guru.'</td>
                    <td>'.$row->tgl_mulai_kerja.'</td>
                    <td>'.$row->pangkat.'</td>
                    <td align="center">'.$row->c1.'</td>
                    <td align="center">'.$row->c2.'</td>
                    <td align="center">'.$row->c3.'</td>
                    <td align="center">'.$row->c4.'</td>
                    <td align="center">'.$row->skor.'</td>
              </tr>';
      }
    $html.='</table>';

    // print_r($html);die;
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->Output('contoh1.pdf', 'I');
?>
