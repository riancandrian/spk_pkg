<div class="content-wrapper">
  <section class="content-header">
    <h1>Laporan Data Siswa</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-book"></i> Laporan</a></li>
      <li class="active">Data Siswa</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <!-- <button type="button" class="btn bg-danger btn-sm btn-flat" ><i class="fa fa-print"></i> Cetak</button> -->
        </div>
      </div>

      <div class="box-body">
        <form class="form-horizontal" method="post" id="f_siswa">
          <div class="form-group">
            <label class="col-md-2 control-label">Tingkat</label>
            <div class="col-md-4">
              <select class="form-control" name="tingkat" required>
                <option value="">Pilih Tingkat..</option>
                <option value="X">X</option>
                <option value="XI">XI</option>
                <option value="XII">XII</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 control-label">Konsentrasi</label>
            <div class="col-md-4">
              <select class="form-control" name="konsentrasi" required>
                <option value="">Pilih Konsentrasi..</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-8">
              <button type="submit" name="submit" class="btn btn-danger"><i class="fa fa-search"></i> Tampilkan</button>
            </div>
          </div>
        </form>
        <br>
        <div class="table-responsive">
          <table class="table table-hover" id="tb_siswa">
            <thead>
              <th width="5%">No.</th>
              <th width="15%">NIP</th>
              <th width="40%">Nama Siswa</th>
              <th width="20%">Kelas</th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('select[name="tingkat"]').change(function(){
      var tingkat = $(this).val();

      $.ajax({
        url : url + 'master/getKelas',
        data: {tingkat: tingkat},
        type: 'POST',
        success: function(result){
          var jsonData = JSON.parse(result);
          $('select[name="konsentrasi"]').empty();
          $('select[name="konsentrasi"]').append("<option value=''>Pilih Kelas..</option>");

          if(jsonData.success){
            $.each(jsonData.data, function(key, val){
              var tr = "<option value='"+val.id_kelas+"'>"+val.konsentrasi+"</option>";

              $('select[name="konsentrasi"]').append(tr);
            });
          }

        }
      });
    });

    $('form#f_siswa').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'laporan/get_siswa',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);
            var nomor = 1;
            $('#tb_siswa tbody').empty();
            if(jsonData.success){
              $.each(jsonData.data, function(key, val){

                var tr = "<tr>\
                            <td>"+(nomor++)+"</td>\
                            <td>"+val.nis+"</td>\
                            <td>"+val.nama+"</td>\
                            <td>"+val.tingkat+' '+val.konsentrasi+"</td>\
                          </tr>";

                $('#tb_siswa tbody').append(tr);

              });
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });
  });
</script>
