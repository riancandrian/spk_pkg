<div class="content-wrapper">
  <section class="content-header">
    <h1>Laporan Data Penilaian</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-book"></i> Laporan</a></li>
      <li class="active">Data Penilaian</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <?php if (isset($data['hasil'])): ?>
            <a href="<?=base_url('cetakan/index').'/'.$data['post_periode'];?>" class="btn bg-danger btn-sm btn-flat" target="_blank"><i class="fa fa-file-pdf-o"></i> Download</a>
            <!-- <button type="button" class="btn bg-danger btn-sm btn-flat" ><i class="fa fa-file-pdf-o"></i> Download</button> -->
          <?php endif; ?>

        </div>
      </div>

      <div class="box-body">
        <form class="form-horizontal" action="<?=base_url('laporan/lap_penilaian')?>" method="post">
          <div class="form-group">
            <label class="col-md-2 control-label">Periode</label>
            <div class="col-md-4">
              <select class="form-control" name="periode">
                <?php
                  foreach ($data['periode'] as $value) {
                    // code...
                    if($value->status == 'aktif'){
                      echo "<option value='".$value->id_periode."' selected>".$value->thn."</option>";
                    }else{
                      echo "<option value='".$value->id_periode."'>".$value->thn."</option>";
                    }
                  }

                ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-8">
              <button type="submit" name="submit" class="btn btn-danger"><i class="fa fa-search"></i> Tampilkan</button>
            </div>
          </div>
        </form>
        <br>
        <div class="table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th width="5%" rowspan="2">No.</th>
                <th width="10%" rowspan="2">NIP</th>
                <th width="20%" rowspan="2">Nama Guru</th>
                <th width="10%" rowspan="2">Tgl. Kerja</th>
                <th width="10%" rowspan="2">Pangkat</th>
                <th width="45%" colspan="5">Score</th>
              </tr>
              <tr>
                <td>Pedagogik</td>
                <td>Kepribadian</td>
                <td>Sosial</td>
                <td>Profesional</td>
                <td>Total</td>
              </tr>
            </thead>
            <tbody>
              <?php

                if(isset($data['hasil'])){

                  $no = 1;
                  foreach ($data['hasil'] as $value) {
                    // code...
                      echo "<tr>
                              <td>".$no."</td>
                              <td>".$value->nip."</td>
                              <td>".$value->nama_guru."</td>
                              <td>".$value->tgl_mulai_kerja."</td>
                              <td>".$value->pangkat."</td>
                              <td>".$value->c1."</td>
                              <td>".$value->c2."</td>
                              <td>".$value->c3."</td>
                              <td>".$value->c4."</td>
                              <td>".$value->skor."</td>
                            </tr>";

                      $no++;
                  }
                }else{
                  echo "<tr>
                          <td colspan='10'>Tidak ada data, silahkan pilih Periode</td>
                        </tr>";
                }

              ?>
            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>
