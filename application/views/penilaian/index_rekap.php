<div class="content-wrapper">
  <section class="content-header">
    <h1>Rekap Hasil Penilaian</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-list"></i> Penilaian</a></li>
      <li class="active">Rekap</li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Hasil Penilaian Pertama</h3>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th rowspan="2">No.</th>
                    <th rowspan="2">NIP</th>
                    <th rowspan="2">Nama Guru</th>
                    <th colspan="4">Kriteria</th>
                  </tr>
                  <tr>
                    <th>Pedagogik</th>
                    <th>Kepribadian</th>
                    <th>Sosial</th>
                    <th>Profesional</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $nomor = 1;

                    foreach ($matrik as $key => $m_row) {
                      echo "<tr>
                              <td>".$nomor."</td>
                              <td>".$m_row->nip."</td>
                              <td>".$m_row->nama_guru."</td>
                              <td>".round($m_row->pedagogik, 3)."</td>
                              <td>".round($m_row->kepribadian, 3)."</td>
                              <td>".round($m_row->sosial, 3)."</td>
                              <td>".round($m_row->profesional, 3)."</td>
                            </tr>";

                      $nomor++;
                    }

                  ?>
                </tbody>
              </table>
            </div>
          </div>

          <div class="box-footer"></div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Normalisasi Data Benefit</h3>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <table class="table table-bordered">
              <thead>
                <tr>
                  <th rowspan="3">No.</th>
                  <th rowspan="3">Nama Guru</th>
                  <th colspan="8">Kriteria</th>
                </tr>
                <tr>
                  <th colspan="2">Pedagogik</th>
                  <th colspan="2">Kepribadian</th>
                  <th colspan="2">Sosial</th>
                  <th colspan="2">Profesional</th>
                </tr>
                <tr>
                  <th>Nilai</th>
                  <th>Normal</th>
                  <th>Nilai</th>
                  <th>Normal</th>
                  <th>Nilai</th>
                  <th>Normal</th>
                  <th>Nilai</th>
                  <th>Normal</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $no = 1;
                  foreach ($matrik as $key => $m_row) {
                    // code...
                    echo "<tr>
                            <td>".$no."</td>
                            <td>".$m_row->nama_guru."</td>
                            <td>".round($m_row->pedagogik, 3)."</td>
                            <td>".round($m_row->pedagogik / $maxmin['max_pedagostik'], 3)."</td>
                            <td>".round($m_row->kepribadian, 3)."</td>
                            <td>".round($m_row->kepribadian / $maxmin['max_kepribadian'], 3)."</td>
                            <td>".round($m_row->sosial, 3)."</td>
                            <td>".round($m_row->sosial / $maxmin['max_sosial'], 3)."</td>
                            <td>".round($m_row->profesional, 3)."</td>
                            <td>".round($m_row->profesional / $maxmin['max_profesional'], 3)."</td>
                          </tr>";

                    $no++;
                  }
                ?>
              </tbody>
            </table>
            </div>
          </div>

          <div class="box-footer"></div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Hasil Normalisasi X Bobot Kriteria</h3>
          </div>

          <div class="box-body">
            <div class="table-responsive">
              <?php

                $nom = 1;
                $isidata = '';

                $this->db->query("DELETE FROM tb_hasil_penilaian where periode = '$periode' ");

                foreach ($matrik as $key => $m_row) {
                  // code...
                  $pedagogik    = round(($m_row->pedagogik / $maxmin['max_pedagostik']) * ($bobot['Pedagogik']/100), 3);
                  $kepribadian  = round(($m_row->kepribadian / $maxmin['max_kepribadian']) * ($bobot['Kepribadian']/100), 3);
                  $sosial       = round(($m_row->sosial / $maxmin['max_sosial']) * ($bobot['Sosial']/100), 3);
                  $profesional  = round(($m_row->profesional / $maxmin['max_profesional']) * ($bobot['Profesional']/100), 3);

                  $total        = $pedagogik + $kepribadian + $sosial + $profesional;

                  //simpan ke database
                  $data         = array('nip' => $m_row->nip
                                  , 'skor' => $total
                                  , 'periode' => $periode
                                  , 'c1' => $pedagogik
                                  , 'c2' => $kepribadian
                                  , 'c3' => $sosial
                                  , 'c4' => $profesional
                                );
                  $this->db->insert('tb_hasil_penilaian', $data);

                  $isidata .= "<tr>
                                <td>".$nom."</td>
                                <td>".$m_row->nama_guru."</td>
                                <td>".$pedagogik."</td>
                                <td>".$kepribadian."</td>
                                <td>".$sosial."</td>
                                <td>".$profesional."</td>
                                <td>".$total."</td>
                               </tr>";
                  $nom++;
                }

                echo "<table class='table table-bordered'>
                        <thead>
                          <tr>
                            <th rowspan='2'>No.</th>
                            <th rowspan='2'>Nama Guru</th>
                            <th colspan='4'>Kriteria</th>
                            <th rowspan='2'>Total</th>
                          </tr>
                          <tr>
                            <th>Pedagogik <br>( ".$bobot['Pedagogik']."% )</th>
                            <th>Kepribadian <br>( ".$bobot['Kepribadian']."% )</th>
                            <th>Sosial <br>( ".$bobot['Sosial']."% )</th>
                            <th>Profesional <br>( ".$bobot['Profesional']."% )</th>
                          </tr>
                        </thead>
                        <tbody>".$isidata."</tbody>
                      </table>";

              ?>
            </div>
          </div>

          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
</div>
