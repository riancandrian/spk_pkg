<div class="content-wrapper">
  <section class="content-header">
    <h1>Soal Kompetensi</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-list"></i> Penilaian</a></li>
      <li>Parameter</li>
      <li>Kompetensi</li>
      <li class="active">Soal Kompetensi</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i> Tambah Soal</button>
        </div>
      </div>

      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-hover" id="tb_soal">
            <thead>
              <th width="5%">No.</th>
              <th width="20%">Kompetensi</th>
              <th width="20%">Kategori</th>
              <th width="35%">Soal</th>
              <th width="20%"></th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" method="post" id="f_soal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Soal</h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
              <label class="col-md-3 control-label">Kompetensi</label>
              <div class="col-md-8">
                <input type="hidden" name="ids" value="">
                <select class="form-control" name="kompetensi">
                  <option value="">Pilih Kompetensi</option>
                  <?php
                    $get = $this->db->get('tb_kompetensi')->result();

                    foreach ($get as $key => $value) {
                      // code...
                      echo "<option value='".$value->id_kompetensi."' >".$value->kompetensi."</option>";
                    }
                  ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Soal</label>
              <div class="col-md-8">
                <!-- <input type="text" class="form-control" name="soal" value=""> -->
                <textarea class="form-control" name="soal" rows="4" cols="80"></textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Kategori</label>
              <div class="col-md-8">
                <select class="form-control" name="kategori">
                  <option value="">Pilih Kategori..</option>
                  <option value="siswa">Siswa</option>
                  <option value="guru">Guru</option>
                </select>

              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    loadData();

    $('form#f_soal').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'penilaian/saveorup_soal',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);

            if(jsonData.success){
                Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
                loadData();
                $('#modal-default').modal('hide');
                $('form#f_soal').trigger('reset');
                $('input[name="id_soal"]').val("");
            }else{
                alert("Data Gagal disimpan");
                $('form#f_soal').triger('reset');
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });
  });

  function loadData(){
    $.ajax({
      url : url + 'penilaian/getSoal',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        $('#tb_soal tbody').empty();
        var nomor = 1;

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){

            btn  = "<button type='button' class='btn btn-xs btn-warning' onclick='getForEdit("+val.id_soal+")'><i class='fa fa-edit'></i> Edit</button>";
            btn2 = "<button type='button' class='btn btn-xs btn-danger' onclick='hapus("+val.id_soal+")'><i class='fa fa-trash'></i> Hapus</button>";

            var tr = "<tr>\
                        <td>"+(nomor++)+"</td>\
                        <td>"+val.kompetensi+"</td>\
                        <td>"+val.kategori+"</td>\
                        <td>"+val.soal+"</td>\
                        <td>"+btn+' '+btn2+"</td>\
                      </tr>";

            $('#tb_soal tbody').append(tr);

          });
        }
      }
    });
  }

  function hapus(ids){
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Anda tidak bisa membatalkan aksi ini!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus saja!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url : url + 'penilaian/hapusSoal',
          data: {ids: ids},
          type: 'POST',
          success: function(result){
            var jsonData = JSON.parse(result);

            if(jsonData.success){
              Swal.fire(
                'Deleted!',
                'Record telah terhapus.',
                'success'
              )

              loadData();
            }
          }
        });
      }
    })
  }

  function getForEdit(ids){
     $.ajax({
       url : url + 'penilaian/getSoal',
       data: {ids: ids},
       type: 'POST',
       success: function(result){
         var jsonData = JSON.parse(result);

         if(jsonData.success){
           $.each(jsonData.data, function(key, val){
             $('input[name="ids"]').val(val.id_soal);
             $('select[name="kompetensi"]').val(val.id_kompetensi);
             $('select[name="kategori"]').val(val.kategori);
             $('textarea[name="soal"]').val(val.soal);
           });
         }

         $('#modal-default').modal('show');
       }
     });
  }
</script>
