<div class="content-wrapper">
  <div class="container">
    <section class="content-header">
      <h1>
        Input Penilaian
      </h1>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">Selamat datang.. </h3>
            </div>

            <form class="form-horizontal" method="post" id="f_penilaian">
              <div class="box-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama Guru</label>
                    <div class="col-md-4">
                      <select class="form-control" name="getGuru">
                        <option value="">Pilih Guru..</option>
                        <?php
                          $get_periode = $this->db->get_where('tb_periode', array('status' => 'aktif' ))->row_array();
                          $periode     = $get_periode['id_periode'];
                          $idpengguna  = $this->session->userdata('user_id');
                          $username    = $this->session->userdata('username');

                          $query = "SELECT *
                                    FROM
                                      tb_guru
                                      WHERE NOT EXISTS ( SELECT *
                                    FROM
                                      tb_penilaian
                                    WHERE
                                      id_pengguna = '".$idpengguna."' AND id_periode = '".$periode."'
                                      AND tb_penilaian.nip = tb_guru.nip)
                                    AND tb_guru.nip != '".$username."' ";
                          $get = $this->db->query($query)->result();

                          foreach ($get as $key => $value) {
                            // code...
                            echo "<option value='".$value->nip."'>".$value->nama_guru."</option>";
                          }
                        ?>
                      </select>
                    </div>
                </div>


                <br>
                <label class="col-md-12">Jawablah kuisioner dibawah ini untuk membantu meningkatkan kualitas Guru</label>

                <table class="table" id="tb_penilaian">
                  <thead>
                    <tr>
                      <th rowspan="2">No.</th>
                      <th rowspan="2">Pertanyaan</th>
                      <th colspan="5">Jawaban</th>
                    </tr>
                    <tr>
                      <th>Sangat Setuju</th>
                      <th>Setuju</th>
                      <th>Kurang Setuju</th>
                      <th>Tidak Setuju</th>
                      <th>Sangat Tidak Setuju</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>

                <div class="form-group">
                  <br>
                  <div class="col-md-12" align="center">
                    <button type="submit" class="btn btn-danger"><i class="fa fa-save"></i> Submit</button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>
<script type="text/javascript">
  var url = "<?=base_url();?>";

  $(document).ready(function(){
    $('select[name="getGuru"]').change(function(){
      loadSoal();
    });

    $('form#f_penilaian').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'raport/save_penilaian',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);

            if(jsonData.success){
                Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
                location.reload();
            }else{
                alert("Data Gagal disimpan");
                $('form#f_kompetensi').triger('reset');
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });

  });

  function loadSoal(){
    $.ajax({
      url : url + 'penilaian/getsoal_fortest',
      success: function(response){
        var jsonData = JSON.parse(response);
        var nomor = 1;
        $('#tb_penilaian tbody').empty();

        $.each(jsonData.data, function(key, val){
          var tr = "<tr>\
                      <td>"+nomor+"</td>\
                      <td>"+val.soal+"\
                          <input type='hidden' name='soal[]' value='"+val.id_soal+"'>\
                      </td>\
                      <td align='center'><input type='radio' name='"+nomor+"' value='5' required></td>\
                      <td align='center'><input type='radio' name='"+nomor+"' value='4'></td>\
                      <td align='center'><input type='radio' name='"+nomor+"' value='3'></td>\
                      <td align='center'><input type='radio' name='"+nomor+"' value='2'></td>\
                      <td align='center'><input type='radio' name='"+nomor+"' value='1'></td>\
                    </tr>";

          $('#tb_penilaian tbody').append(tr);
          nomor++;

        });
      }
    });
  }

</script>
