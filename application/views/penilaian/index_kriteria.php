<div class="content-wrapper">
  <section class="content-header">
    <h1>Kriteria Penilaian</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-list"></i> Penilaian</a></li>
      <li class="active">Kriteria</li>
    </ol>
  </section>

  <section class="content">
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn bg-danger btn-sm btn-flat" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i> Tambah Kriteria</button>
        </div>
      </div>

      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-hover" id="tb_kriteria">
            <thead>
              <th width="5%">No.</th>
              <th width="45%">Kriteria</th>
              <th width="20%">Bobot</th>
              <th width="10%">Status</th>
              <th width="20%"></th>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>

      <div class="box-footer"></div>
    </div>
  </section>
</div>

<div class="modal fade" id="modal-default">
  <div class="modal-dialog">
    <div class="modal-content">
      <form class="form-horizontal" method="post" id="f_kriteria">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Tambah Kriteria</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label class="col-md-3 control-label">Kriteria</label>
            <div class="col-md-8">
              <input type="hidden" class="form-control" name="idk">
              <input type="text" class="form-control" name="kriteria">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Bobot</label>
            <div class="col-md-8">
              <input type="text" class="form-control" name="bobot">
            </div>
          </div>

          <div class="form-group">
              <label class="col-md-3 control-label">Status</label>
              <div class="col-md-8">
                <select class="form-control" name="status">
                  <option value="">Pilih Status..</option>
                  <option value="benefit">Benefit</option>
                  <option value="cost">Cost</option>
                </select>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left btn-sm" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="<?=base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url(); ?>assets/dist/js/sweetalert2.all.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    loadData();

    $('form#f_kriteria').submit(function(e){
      e.preventDefault();
      var formData = new FormData(this);

      $.ajax({
        url: url + 'penilaian/saveorup_kriteria',
        type: 'POST',
        data: formData,
        success: function (data) {
            var jsonData = JSON.parse(data);

            if(jsonData.success){
                Swal.fire("Selamat !", "Data berhasil disimpan !", "success");
                loadData();
                $('#modal-default').modal('hide');
                $('form#f_kriteria').trigger('reset');
                $('input[name="idk"]').val("");
            }else{
                alert("Data Gagal disimpan");
                $('form#f_kriteria').triger('reset');
            }
        },
        cache: false,
        contentType: false,
        processData: false
      });
    });
  });

  function loadData(){
    $.ajax({
      url : url + 'penilaian/getKriteria',
      type: 'POST',
      success: function(result){
        var jsonData = JSON.parse(result);

        $('#tb_kriteria tbody').empty();
        var nomor = 1;

        if(jsonData.success){
          $.each(jsonData.data, function(key, val){

            btn  = "<button type='button' class='btn btn-xs btn-warning' onclick='getForEdit("+val.id_kriteria+")'><i class='fa fa-edit'></i> Edit</button>";
            btn2 = "<button type='button' class='btn btn-xs btn-danger' onclick='hapus("+val.id_kriteria+")'><i class='fa fa-trash'></i> Hapus</button>";

            if(val.status == 'benefit'){
              label = '<small class="label bg-green">Benefit</small>';
            }else{
              label = '<small class="label bg-red">Cost</small>';
            }

            var tr = "<tr>\
                        <td>"+(nomor++)+"</td>\
                        <td>"+val.kriteria+"</td>\
                        <td>"+val.bobot+"</td>\
                        <td>"+label+"</td>\
                        <td>"+btn+' '+btn2+"</td>\
                      </tr>";

            $('#tb_kriteria tbody').append(tr);

          });
        }
      }
    });
  }

  function hapus(idk){
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Anda tidak bisa membatalkan aksi ini!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, hapus saja!'
    }).then((result) => {
      if (result.value) {
        $.ajax({
          url : url + 'penilaian/hapusKriteria',
          data: {idk: idk},
          type: 'POST',
          success: function(result){
            var jsonData = JSON.parse(result);

            if(jsonData.success){
              Swal.fire(
                'Deleted!',
                'Record telah terhapus.',
                'success'
              )

              loadData();
            }
          }
        });
      }
    })
  }

  function getForEdit(idk){
     $.ajax({
       url : url + 'penilaian/getKriteria',
       data: {idk: idk},
       type: 'POST',
       success: function(result){
         var jsonData = JSON.parse(result);

         if(jsonData.success){
           $.each(jsonData.data, function(key, val){
             $('input[name="idk"]').val(val.id_kriteria);
             $('input[name="kriteria"]').val(val.kriteria);
             $('input[name="bobot"]').val(val.bobot);
             $('select[name="status"]').val(val.status);
           });
         }

         $('#modal-default').modal('show');
       }
     });
  }
</script>
