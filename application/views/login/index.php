<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SPK PKG | Log in</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style media="screen">
    .login-page {
      background-image: url("<?=base_url('assets/dist/img/header-bg.jpg');?>");
      background-repeat: no-repeat;
      background-size: cover;
      /* background-position: right top;
      background-attachment: fixed; */
    }
  </style>

</head>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>SMAN 24</b> Bandung</a>
  </div>

  <?php

    if(isset($msg)){
      echo '
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Maaf !</h4>
          NIK / NIP yang anda masukan salah. Mohon periksa kembali !
        </div>
      ';
    }

  ?>


  <div class="login-box-body">
    <p class="login-box-msg"><b>SISTEM PENILAIAN KINERJA GURU</b></p>

    <form action="<?=base_url(); ?>login/aksi_login" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="NIK / NUPTK" name="username" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <button type="submit" class="btn btn-danger btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
  </div>
</div>

<script src="<?=base_url('assets/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<script src="<?=base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<script>

</script>
</body>
</html>
