-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 11, 2019 at 02:16 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_spkpkg`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_company`
--

CREATE TABLE IF NOT EXISTS `tb_company` (
  `nama` varchar(100) NOT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_company`
--

INSERT INTO `tb_company` (`nama`, `telp`, `alamat`) VALUES
('SMAN 24 Bandung', '0211111', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_guru`
--

CREATE TABLE IF NOT EXISTS `tb_guru` (
  `nip` varchar(100) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `pangkat` varchar(100) DEFAULT NULL,
  `tgl_mulai_kerja` date DEFAULT NULL,
  `status` enum('active','nonactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_guru`
--

INSERT INTO `tb_guru` (`nip`, `nama_guru`, `pangkat`, `tgl_mulai_kerja`, `status`) VALUES
('0060756658200043', 'Engkus Kusnadi', 'tetap', '2019-05-01', 'active'),
('0060756658200047', 'Muhammad Husein', 'honorer', '2019-05-01', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hasil_penilaian`
--

CREATE TABLE IF NOT EXISTS `tb_hasil_penilaian` (
`id_hasil` int(5) NOT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `skor` float DEFAULT NULL,
  `periode` int(3) DEFAULT NULL,
  `c1` float DEFAULT NULL,
  `c2` float DEFAULT NULL,
  `c3` float DEFAULT NULL,
  `c4` float DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_hasil_penilaian`
--

INSERT INTO `tb_hasil_penilaian` (`id_hasil`, `nip`, `skor`, `periode`, `c1`, `c2`, `c3`, `c4`) VALUES
(21, '0060756658200043', 0.998, 3, 0.148, 0.25, 0.25, 0.35),
(22, '0060756658200047', 0.955, 3, 0.15, 0.234, 0.244, 0.327);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE IF NOT EXISTS `tb_kelas` (
`id_kelas` int(3) NOT NULL,
  `tingkat` varchar(3) NOT NULL,
  `konsentrasi` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `tingkat`, `konsentrasi`) VALUES
(1, 'X', 'Otomotif 1'),
(2, 'X', 'Otomotif 2'),
(3, 'X', 'Otomotif 3'),
(4, 'X', 'Otomotif 4'),
(5, 'X', 'Otomotif 5'),
(6, 'X', 'Pemesinan 1'),
(7, 'X', 'Pemesinan 2'),
(8, 'X', 'Pemesinan 3'),
(9, 'X', 'Pemesinan 4'),
(10, 'X', 'Pemesinan 5'),
(11, 'X', 'Multimedia 1'),
(12, 'X', 'Multimedia 2'),
(13, 'X', 'Multimedia 3'),
(14, 'X', 'Multimedia 4'),
(16, 'X', 'Persiapan Grafika 1'),
(17, 'X', 'Persiapan Grafika 2'),
(18, 'X', 'Persiapan Grafika 3'),
(19, 'X', 'Persiapan Grafika 4'),
(20, 'X', 'Multimedia 5'),
(21, 'X', 'Persiapan Grafika 5');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kompetensi`
--

CREATE TABLE IF NOT EXISTS `tb_kompetensi` (
`id_kompetensi` int(3) NOT NULL,
  `id_kriteria` int(3) NOT NULL,
  `kompetensi` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kompetensi`
--

INSERT INTO `tb_kompetensi` (`id_kompetensi`, `id_kriteria`, `kompetensi`) VALUES
(2, 2, 'Menguasai Karakteristik peserta didik.'),
(3, 3, 'Pribadi Diri Guru'),
(4, 4, 'Kedermawanan Guru'),
(5, 5, 'Profesionalisme Guru');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kriteria`
--

CREATE TABLE IF NOT EXISTS `tb_kriteria` (
`id_kriteria` int(4) NOT NULL,
  `kriteria` varchar(100) NOT NULL,
  `bobot` int(3) NOT NULL,
  `status` enum('benefit','cost') NOT NULL,
  `kelompok` enum('Siswa','Guru') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kriteria`
--

INSERT INTO `tb_kriteria` (`id_kriteria`, `kriteria`, `bobot`, `status`, `kelompok`) VALUES
(2, 'Pedagogik', 15, 'benefit', 'Siswa'),
(3, 'Kepribadian', 25, 'benefit', 'Siswa'),
(4, 'Sosial', 25, 'benefit', 'Siswa'),
(5, 'Profesional', 35, 'benefit', 'Siswa');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengguna`
--

CREATE TABLE IF NOT EXISTS `tb_pengguna` (
`id_pengguna` int(5) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `role` enum('siswa','guru','admin') DEFAULT NULL,
  `status` enum('active','blocked') DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pengguna`
--

INSERT INTO `tb_pengguna` (`id_pengguna`, `username`, `password`, `nama_lengkap`, `role`, `status`) VALUES
(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'admin', 'active'),
(4, '0060756658200043', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Engkus Kusnadi', 'guru', 'active'),
(5, '151610002', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Adhitia Akbar Rahmansyah', 'siswa', 'active'),
(6, '151610001', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Adel Wili Afandix', 'siswa', 'active'),
(7, '0060756658200047', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Muhammad Husein', 'guru', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penilaian`
--

CREATE TABLE IF NOT EXISTS `tb_penilaian` (
`id_penilaian` int(11) NOT NULL,
  `id_soal` int(5) DEFAULT NULL,
  `id_periode` int(3) DEFAULT NULL,
  `nilai` int(3) DEFAULT NULL,
  `id_pengguna` int(5) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_penilaian`
--

INSERT INTO `tb_penilaian` (`id_penilaian`, `id_soal`, `id_periode`, `nilai`, `id_pengguna`, `nip`) VALUES
(1, 1, 3, 5, 5, '0060756658200043'),
(2, 2, 3, 5, 5, '0060756658200043'),
(3, 3, 3, 4, 5, '0060756658200043'),
(4, 4, 3, 4, 5, '0060756658200043'),
(5, 6, 3, 3, 5, '0060756658200043'),
(6, 7, 3, 3, 5, '0060756658200043'),
(7, 8, 3, 4, 5, '0060756658200043'),
(8, 9, 3, 4, 5, '0060756658200043'),
(9, 10, 3, 5, 5, '0060756658200043'),
(10, 11, 3, 5, 5, '0060756658200043'),
(11, 12, 3, 4, 5, '0060756658200043'),
(12, 13, 3, 4, 5, '0060756658200043'),
(13, 14, 3, 3, 5, '0060756658200043'),
(14, 15, 3, 3, 5, '0060756658200043'),
(15, 16, 3, 4, 5, '0060756658200043'),
(16, 18, 3, 4, 5, '0060756658200043'),
(17, 19, 3, 5, 5, '0060756658200043'),
(18, 20, 3, 5, 5, '0060756658200043'),
(51, 21, 3, 5, 7, '0060756658200043'),
(52, 22, 3, 5, 7, '0060756658200043'),
(53, 23, 3, 5, 7, '0060756658200043'),
(54, 24, 3, 5, 7, '0060756658200043'),
(55, 25, 3, 5, 7, '0060756658200043'),
(56, 26, 3, 5, 7, '0060756658200043'),
(57, 27, 3, 5, 7, '0060756658200043'),
(58, 28, 3, 5, 7, '0060756658200043'),
(59, 29, 3, 5, 7, '0060756658200043'),
(60, 30, 3, 5, 7, '0060756658200043'),
(61, 31, 3, 5, 7, '0060756658200043'),
(62, 32, 3, 5, 7, '0060756658200043'),
(63, 33, 3, 5, 7, '0060756658200043'),
(64, 34, 3, 5, 7, '0060756658200043'),
(65, 35, 3, 5, 7, '0060756658200043'),
(66, 36, 3, 5, 7, '0060756658200043'),
(67, 1, 3, 4, 6, '0060756658200043'),
(68, 2, 3, 4, 6, '0060756658200043'),
(69, 3, 3, 4, 6, '0060756658200043'),
(70, 4, 3, 3, 6, '0060756658200043'),
(71, 6, 3, 3, 6, '0060756658200043'),
(72, 7, 3, 4, 6, '0060756658200043'),
(73, 8, 3, 4, 6, '0060756658200043'),
(74, 9, 3, 3, 6, '0060756658200043'),
(75, 10, 3, 5, 6, '0060756658200043'),
(76, 11, 3, 4, 6, '0060756658200043'),
(77, 12, 3, 5, 6, '0060756658200043'),
(78, 13, 3, 4, 6, '0060756658200043'),
(79, 14, 3, 4, 6, '0060756658200043'),
(80, 15, 3, 5, 6, '0060756658200043'),
(81, 16, 3, 4, 6, '0060756658200043'),
(82, 18, 3, 4, 6, '0060756658200043'),
(83, 19, 3, 4, 6, '0060756658200043'),
(84, 20, 3, 4, 6, '0060756658200043'),
(85, 1, 3, 4, 6, '0060756658200047'),
(86, 2, 3, 4, 6, '0060756658200047'),
(87, 3, 3, 3, 6, '0060756658200047'),
(88, 4, 3, 5, 6, '0060756658200047'),
(89, 6, 3, 4, 6, '0060756658200047'),
(90, 7, 3, 4, 6, '0060756658200047'),
(91, 8, 3, 4, 6, '0060756658200047'),
(92, 9, 3, 5, 6, '0060756658200047'),
(93, 10, 3, 4, 6, '0060756658200047'),
(94, 11, 3, 4, 6, '0060756658200047'),
(95, 12, 3, 4, 6, '0060756658200047'),
(96, 13, 3, 5, 6, '0060756658200047'),
(97, 14, 3, 4, 6, '0060756658200047'),
(98, 15, 3, 4, 6, '0060756658200047'),
(99, 16, 3, 4, 6, '0060756658200047'),
(100, 18, 3, 4, 6, '0060756658200047'),
(101, 19, 3, 5, 6, '0060756658200047'),
(102, 20, 3, 5, 6, '0060756658200047'),
(103, 1, 3, 5, 5, '0060756658200047'),
(104, 2, 3, 5, 5, '0060756658200047'),
(105, 3, 3, 4, 5, '0060756658200047'),
(106, 4, 3, 4, 5, '0060756658200047'),
(107, 6, 3, 4, 5, '0060756658200047'),
(108, 7, 3, 4, 5, '0060756658200047'),
(109, 8, 3, 4, 5, '0060756658200047'),
(110, 9, 3, 3, 5, '0060756658200047'),
(111, 10, 3, 3, 5, '0060756658200047'),
(112, 11, 3, 4, 5, '0060756658200047'),
(113, 12, 3, 4, 5, '0060756658200047'),
(114, 13, 3, 4, 5, '0060756658200047'),
(115, 14, 3, 4, 5, '0060756658200047'),
(116, 15, 3, 3, 5, '0060756658200047'),
(117, 16, 3, 4, 5, '0060756658200047'),
(118, 18, 3, 4, 5, '0060756658200047'),
(119, 19, 3, 3, 5, '0060756658200047'),
(120, 20, 3, 4, 5, '0060756658200047'),
(121, 21, 3, 5, 4, '0060756658200047'),
(122, 22, 3, 5, 4, '0060756658200047'),
(123, 23, 3, 5, 4, '0060756658200047'),
(124, 24, 3, 4, 4, '0060756658200047'),
(125, 25, 3, 4, 4, '0060756658200047'),
(126, 26, 3, 4, 4, '0060756658200047'),
(127, 27, 3, 4, 4, '0060756658200047'),
(128, 28, 3, 4, 4, '0060756658200047'),
(129, 29, 3, 4, 4, '0060756658200047'),
(130, 30, 3, 4, 4, '0060756658200047'),
(131, 31, 3, 4, 4, '0060756658200047'),
(132, 32, 3, 5, 4, '0060756658200047'),
(133, 33, 3, 5, 4, '0060756658200047'),
(134, 34, 3, 5, 4, '0060756658200047'),
(135, 35, 3, 5, 4, '0060756658200047'),
(136, 36, 3, 5, 4, '0060756658200047');

-- --------------------------------------------------------

--
-- Table structure for table `tb_periode`
--

CREATE TABLE IF NOT EXISTS `tb_periode` (
`id_periode` int(3) NOT NULL,
  `thn` varchar(4) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_periode`
--

INSERT INTO `tb_periode` (`id_periode`, `thn`, `start`, `end`, `status`) VALUES
(3, '2019', '2019-01-01', '2019-12-31', 'aktif'),
(4, '2018', '2018-01-01', '2018-12-31', 'nonaktif');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE IF NOT EXISTS `tb_siswa` (
  `nis` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_kelas` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`nis`, `nama`, `id_kelas`) VALUES
('151610002', 'Adhitia Akbar Rahmansyah', 11),
('151610001', 'Adel Wili Afandix', 16);

-- --------------------------------------------------------

--
-- Table structure for table `tb_soal_kompetensi`
--

CREATE TABLE IF NOT EXISTS `tb_soal_kompetensi` (
`id_soal` int(5) NOT NULL,
  `id_kompetensi` int(3) NOT NULL,
  `soal` text NOT NULL,
  `kategori` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_soal_kompetensi`
--

INSERT INTO `tb_soal_kompetensi` (`id_soal`, `id_kompetensi`, `soal`, `kategori`) VALUES
(1, 2, 'Guru dapat mengidentifikasi karakteristik belajar setiap peserta didik di kelas', 'siswa'),
(2, 2, 'Guru memastikan bahwa semua peserta didik mendapatkan kesempatan yang sama untuk berpartisipasi aktif dalam kegiatan pembelajaran', 'siswa'),
(3, 2, 'Guru dapat mengatur kelas untuk memberikan kesempatan belajar yang sama pada semua peserta didik dengan kelainan fisik dan kemampuan belajar yang berbeda.', 'siswa'),
(4, 2, 'Guru mencoba mengetahui penyebab penyimpangan perilaku peserta didik untuk mencegah agar perilaku tersebut tidak merugikan peserta didik lainnya', 'siswa'),
(6, 2, 'Guru membantu mengembangkan potensi dan mengatasi kekurangan peserta didik.', 'siswa'),
(7, 3, 'Guru bertingkah laku sopan dalam berbicara, berpenampilan, dan berbuat terhadap semua peserta didik, orang tua, dan teman sejawat', 'siswa'),
(8, 3, 'Guru berperilaku baik untuk mencitrakan nama baik sekolah.', 'siswa'),
(9, 3, 'Guru bersikap dewasa dalam menerima masukan dari peserta didik dan memberikan kesempatan kepada peserta didik untuk berpartisipasi dalam proses pembelajaran.', 'siswa'),
(10, 3, 'Guru mampu mengelola pembelajaran yang membuktikan bahwa guru dihormati oleh peserta didik, sehingga semua peserta didik selalu memperhatikan guru dan berpartisipasi aktif dalam proses pembelajaran.', 'siswa'),
(11, 3, 'Guru sering berinteraksi dengan peserta didik dan tidak membatasi perhatiannya hanya pada kelompok tertentu (misalnya: peserta didik yang pandai, kaya, berasal dari daerah yang sama dengan guru).', 'siswa'),
(12, 4, 'Guru menggunakan pertanyaan untuk mengetahui pemahaman dan menjaga partisipasi peserta didik, termasuk memberikan pertanyaan terbuka yang menuntut peserta didik untuk menjawab dengan ide dan pengetahuan mereka.', 'siswa'),
(13, 4, 'Guru menanggapinya pertanyaan peserta didik secara tepat, benar, dan mutakhir, sesuai tujuan pembelajaran dan isi kurikulum, tanpa mempermalukannya.', 'siswa'),
(14, 4, 'Guru menyajikan kegiatan pembelajaran yang dapat menumbuhkan kerja sama yang baik antar pesertadidik.', 'siswa'),
(15, 4, 'Guru mendengarkan dan memberikan perhatian terhadap semua jawaban peserta didik baik yang benar maupun yang dianggap salah untuk mengukur tingkat pemahaman peserta didik.', 'siswa'),
(16, 4, 'Guru memberikan perhatian terhadap pertanyaan peserta didik dan meresponnya secara lengkap dan relevan untuk menghilangkan kebingungan pada peserta didik.', 'siswa'),
(18, 5, 'Guru melakukan pemetaan standar kompetensi dan kompetensi dasar untuk mata pelajaran yang diampunya, untuk mengidentifikasi materi pembelajaran yang dianggap sulit, melakukan perencanaan dan pelaksanaan pembelajaran, dan memperkirakan alokasi waktu yang diperlukan.', 'siswa'),
(19, 5, 'Guru menyertakan informasi yang tepat dan mutakhir di dalam perencanaan dan pelaksanaan pembelajaran.', 'siswa'),
(20, 5, 'Guru menyusun materi, perencanaan dan pelaksanaan pembelajaran yang berisi informasi yang tepat, mutakhir, dan yang membantu peserta didik untuk memahami konsep materi pembelajaran', 'siswa'),
(21, 5, 'Presensi / Kehadiran', 'guru'),
(22, 3, 'Kedisiplinan Aturan', 'guru'),
(23, 3, 'Kedisiplinan Aturan', 'guru'),
(24, 5, 'Tanggungjawab dan Produktifitas', 'guru'),
(25, 4, 'Interaksi Sosial', 'guru'),
(26, 2, 'Motivasi dan Pengembangan diri', 'guru'),
(27, 3, 'Inovatif', 'guru'),
(28, 3, 'Responsif dan Inisiatif', 'guru'),
(29, 5, 'Fleksibilitas dalam tugas lain', 'guru'),
(30, 2, 'Komunikasi', 'guru'),
(31, 3, 'Team work', 'guru'),
(32, 3, 'Kejujuran', 'guru'),
(33, 3, 'Ramah dan Santun', 'guru'),
(34, 5, 'Estetika berpakaian', 'guru'),
(35, 2, 'Efektif dalam menggunakan Gadget', 'guru'),
(36, 2, 'Kebersihan dan Kerapihan Kelas', 'guru');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_guru`
--
ALTER TABLE `tb_guru`
 ADD PRIMARY KEY (`nip`), ADD UNIQUE KEY `nip` (`nip`);

--
-- Indexes for table `tb_hasil_penilaian`
--
ALTER TABLE `tb_hasil_penilaian`
 ADD PRIMARY KEY (`id_hasil`), ADD KEY `FK_tb_hasil_penilaian_tb_guru_nip` (`nip`);

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
 ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_kompetensi`
--
ALTER TABLE `tb_kompetensi`
 ADD PRIMARY KEY (`id_kompetensi`), ADD KEY `FK_tb_kompetensi_tb_kriteria_id_kriteria` (`id_kriteria`);

--
-- Indexes for table `tb_kriteria`
--
ALTER TABLE `tb_kriteria`
 ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `tb_pengguna`
--
ALTER TABLE `tb_pengguna`
 ADD PRIMARY KEY (`id_pengguna`);

--
-- Indexes for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
 ADD PRIMARY KEY (`id_penilaian`), ADD KEY `tb_penilaian_tb_soal_kompetensi_FK` (`id_soal`), ADD KEY `tb_penilaian_tb_periode_FK` (`id_periode`), ADD KEY `tb_penilaian_tb_pengguna_FK` (`id_pengguna`);

--
-- Indexes for table `tb_periode`
--
ALTER TABLE `tb_periode`
 ADD PRIMARY KEY (`id_periode`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
 ADD PRIMARY KEY (`id_kelas`), ADD UNIQUE KEY `nis` (`nis`);

--
-- Indexes for table `tb_soal_kompetensi`
--
ALTER TABLE `tb_soal_kompetensi`
 ADD PRIMARY KEY (`id_soal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_hasil_penilaian`
--
ALTER TABLE `tb_hasil_penilaian`
MODIFY `id_hasil` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
MODIFY `id_kelas` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tb_kompetensi`
--
ALTER TABLE `tb_kompetensi`
MODIFY `id_kompetensi` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_kriteria`
--
ALTER TABLE `tb_kriteria`
MODIFY `id_kriteria` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_pengguna`
--
ALTER TABLE `tb_pengguna`
MODIFY `id_pengguna` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
MODIFY `id_penilaian` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `tb_periode`
--
ALTER TABLE `tb_periode`
MODIFY `id_periode` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_soal_kompetensi`
--
ALTER TABLE `tb_soal_kompetensi`
MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_hasil_penilaian`
--
ALTER TABLE `tb_hasil_penilaian`
ADD CONSTRAINT `FK_tb_hasil_penilaian_tb_guru_nip` FOREIGN KEY (`nip`) REFERENCES `tb_guru` (`nip`);

--
-- Constraints for table `tb_kompetensi`
--
ALTER TABLE `tb_kompetensi`
ADD CONSTRAINT `FK_tb_kompetensi_tb_kriteria_id_kriteria` FOREIGN KEY (`id_kriteria`) REFERENCES `tb_kriteria` (`id_kriteria`);

--
-- Constraints for table `tb_penilaian`
--
ALTER TABLE `tb_penilaian`
ADD CONSTRAINT `tb_penilaian_tb_pengguna_FK` FOREIGN KEY (`id_pengguna`) REFERENCES `tb_pengguna` (`id_pengguna`),
ADD CONSTRAINT `tb_penilaian_tb_periode_FK` FOREIGN KEY (`id_periode`) REFERENCES `tb_periode` (`id_periode`),
ADD CONSTRAINT `tb_penilaian_tb_soal_kompetensi_FK` FOREIGN KEY (`id_soal`) REFERENCES `tb_soal_kompetensi` (`id_soal`);

--
-- Constraints for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
ADD CONSTRAINT `FK_tb_siswa_tb_kelas_id_kelas` FOREIGN KEY (`id_kelas`) REFERENCES `tb_kelas` (`id_kelas`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
